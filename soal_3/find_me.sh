#!/bin/bash

extract_message() {
    for file in *.jpg; do
        if [ -f "$file" ]; then
            echo "[$(date +"%d/%m/%y %T")] [EXTRACTING] [$file]" >> image.log
            steghide extract -sf "$file" -p "" -xf extracted.txt

            if [ $? -eq 0 ]; then
                echo "[$(date +"%d/%m/%y %T")] [FOUND] [$file]" >> image.log

                decoded_message=$(base64 -d < extracted.txt | tr -d '\0' 2>/dev/null)

                if [[ $decoded_message =~ ^http ]]; then
                    echo "URL ditemukan: $decoded_message"


                    wget "$decoded_message"

                    exit 0
                else
                    # Jika pesan tidak mengandung URL, hapus file txt
                    rm extracted.txt
                fi
            else
                # Jika ekstraksi gagal, hapus file txt
                rm extracted.txt
            fi
        fi
    done
}

# Melakukan pengecekan setiap 1 detik
while true; do
    extract_message
    sleep 1
done
