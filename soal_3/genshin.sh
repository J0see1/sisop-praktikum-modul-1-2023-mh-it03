#!/bin/bash

# Download file dari URL
wget -O genshin.zip "https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2"

# Unzip file genshin.zip
unzip "genshin.zip"
unzip "genshin_character.zip" -d "genshin_character"

# Loop untuk mendekripsi nama file dan merename sesuai format
while IFS=',' read -r nama_region elemen senjata; do
    for encoded_file in genshin_character/*.jpg; do
        # Extract the base name of the file without extension
        encoded_name=$(basename "$encoded_file" .jpg)
        decoded_name=$(echo "$encoded_name" | base64 -d)

        if [ "$decoded_name" = "$nama_region" ]; then
            new_file="$nama_region - $elemen - $senjata.jpg"
            mv "$encoded_file" "genshin_character/$new_file"

            # Baca data karakter dari list_character.csv
            character=$(awk -F"," -v name="$decoded_name" '$1 == name {print}' list_character.csv)

            if [ -n "$character" ]; then
                region=$(echo "$character" | awk -F"," '{print $2}')
                new_name=$(echo "$character" | awk -F"," '{print $1"-"$2"-"$3"-"$4}' | sed 's/[^A-Za-z0-9._-]//g')
                mkdir -p "genshin_character/$region"
		mv "genshin_character/$new_file_name" "genshin_character/$region/$new_name"
            fi
        fi
    done
done < list_character.csv

# Inisialisasi associative array untuk menghitung jumlah pengguna senjata
declare -A weapon_counts

# Loop melalui semua folder region
for region_dir in genshin_character/*; do
    if [ -d "$region_dir" ]; then
        region=$(basename "$region_dir")

        # Loop melalui karakter dalam setiap folder region
        for character_file in "$region_dir"/*; do
            if [ -f "$character_file" ]; then
                character_name=$(basename "$character_file" .jpg)
                senjata=$(echo "$character_name" | awk -F"-" '{print $3}')

                # Memeriksa apakah weapon_name tidak kosong
                if [ -n "$senjata" ]; then
                    ((weapon_counts["$senjata"]++))
                fi
            fi
        done
    fi
done

# Menampilkan jumlah pengguna untuk setiap senjata
echo "Jumlah pengguna untuk setiap senjata:"
for weapon in "${!weapon_counts[@]}"; do
    echo "[$weapon] : ${weapon_counts[$weapon]}"
done

# Menghapus file yang tidak digunakan
rm -f genshin_character.zip 
rm -f list_character.csv 
rm -f genshin.zip
