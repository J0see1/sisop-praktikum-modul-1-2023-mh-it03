#!/bin/bash

#melihat pengguna user sekarang
user=$dave

log_dir="/home/dave/log/"

timestamp=$(date +"%Y%m%d%H%M%S")

#menyimpan file dalam log
log_file="${log_dir}metrics_${timestamp}.log"


#menggunakan perintah free untuk mendapat info swap dan RAM
ram=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7}')
swap=$(free -m | awk 'NR==3 {print $2","$3","$4}')

#menyimpan ukuran directory yang ditentukan
target_path="/home/dave/"
path_size=$(du -sh "$target_path" | awk '{print $1}')

#memberikan output dalam format csv
echo "timestamp $timestamp" >> "$log_file"
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "$log_file"
echo "$ram,$swap,$target_path,$path_size" >> "$log_file"

chmod 600 "$log_file"

#crontab config
#* * * * * /Home/dave/praktikum-sisop/soal_4/minute_log.sh

