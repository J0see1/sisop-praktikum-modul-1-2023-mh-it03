#!/bin/bash

#directory tempat file log tergenerate dari minute_log.sh
log_dir="/home/dave/log/"

#format timestamp YYYYMMDDHH
timestamp=$(date +"%Y%m%d%H")

#nama file aggregasi saat ini
file_agg="metrics_agg_${timestamp}.log"

#mencari semua file log yang akan diaggregasi
log_files=$(ls ${log_dir}metrics_*.log)

#inisialisasi variabel minimum, maksimum, total, dan rata-rata
minimum=9999999
maksimum=0
total=0
count=0

#loop pada file log
for file in $log_files; do
    # Mengambil nilai dari file log
    value=$(tail -n 1 $file | awk -F',' '{print $2}')  # Ubah sesuai dengan kolom yang ingin dihitung

    # Menghitung maksimum
    if (( $(echo "$value > $maksimum" | bc -l) )); then
        maksimum=$value
    fi

    # Menghitung minimum
    if (( $(echo "$value < $minimum" | bc -l) )); then
        minimum=$value
    fi

    # Menambahkan nilai untuk rata-rata
    total=$(echo "$total + $value" | bc -l)
    ((count++))
done

# Menghitung rata-rata
average=$(echo "scale=2; $total / $count" | bc -l)

#simpan ke file aggregasi
echo "maksimum $maksimum" > "$file_agg"
echo "minimum $minimum" >> "$file_agg"
echo "average $average" >> "$file_agg"

chmod 600 "$log_files"

#crontab config
#0 1 * * * /Home/dave/praktikum-sisop_/soal_4/aggregate_minutes_to_hourly_log.sh
