#!/bin/bash
if [ ! -d "playlist.csv" ]; then
wget -O playlist.csv "https://drive.google.com/u/0/uc?id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp&export=download"
fi

echo "top 5 lagu hip hop"
echo "======================================================="
grep "hip hop" playlist.csv | sort -t',' -k15 -n -r | head -n 5
echo "=======================================================\n"
echo "top 5 lagu John Mayer dengan popularity terendah"
echo "======================================================="
grep "John Mayer" playlist.csv | sort -t',' -k15  | head -n 5
echo "=======================================================\n"
echo "top 10 lagu terbaik di tahun 2004"
echo "======================================================="
grep "2004" playlist.csv | sort -t',' -k15 -r -n | head -n 10
echo "=======================================================\n"
echo "top 5 lagu terbaik ibu Sri"
echo "======================================================="
grep "Sri" playlist.csv | sort -t',' -k15 -r -n | tail -n 5
echo "=======================================================\n"
