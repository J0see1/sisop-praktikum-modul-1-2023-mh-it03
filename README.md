# sisop-praktikum-modul-1-2023-mh-it03

Pengerjaan soal shift sistem operasi modul 1 oleh IT03

# Anggota

| Nama                            | NRP          |
| ------------------------------- | ------------ |
| Marcelinus Alvinanda Chrisantya | `5027221012` |
| George David Nebore             | `5027221043` |
| Angella Christie                | `5027221047` |

# Konten

- [Soal 1](#soal-1)
  - [Soal 1a](#soal-1a)
  - [Soal 1b](#soal-1b)
  - [Soal 1c](#soal-1c)
  - [Soal 1d](#soal-1d)
- [Soal 2](#soal-2)

- [Soal 3](#soal-3)

- [Soal 4](#soal-4)

# Soal 1

Farrel ingin membuat sebuah playlist yang sangat edgy di tahun ini. Farrel ingin playlistnya banyak disukai oleh orang-orang lain. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa data untuk membuat playlist terbaik di dunia. Untung saja Farrel menemukan file playlist.csv yang berisi top lagu pada spotify beserta genrenya. <br>

1. Farrel ingin memasukan lagu yang bagus dengan genre hip hop. Oleh karena itu, Farrel perlu melakukan testi terlebih dahulu. Tampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity.<br>
1. Karena Farrel merasa kurang dengan lagu tadi, dia ingin mencari 5 lagu yang paling rendah diantara lagu ciptaan John Mayer berdasarkan popularity.<br>
1. Karena Farrel takut lagunya kurang enak didengar dia ingin mencari lagu lagi, cari 10 lagu pada tahun 2004 dengan rank popularity tertinggi<br>
1. Farrel sangat suka dengan lagu paling keren dan edgy di dunia. Lagu tersebut diciptakan oleh ibu Sri. Bantu Farrel mencari lagu tersebut dengan kata kunci nama pencipta lagu tersebut.

# Analisis

<p>	Praktikan diinstruksikan untuk membuat bash script yang dapat mengurutkan lagu di dalam file playlist.csv ( yang didownload melalui bash script ) sesuai dengan poin-poin yang sudah ditetapkan.<p>

```Bash
#!/bin/bash
if [ ! -d "playlist.csv" ]; then
wget -O playlist.csv "https://drive.google.com/u/0/uc?id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp&export=download"
fi

echo "top 5 lagu hip hop"
echo "======================================================="
grep "hip hop" playlist.csv | sort -t',' -k15 -n -r | head -n 5
echo "=======================================================\n"
echo "top 5 lagu John Mayer dengan popularity terendah"
echo "======================================================="
grep "John Mayer" playlist.csv | sort -t',' -k15  | head -n 5
echo "=======================================================\n"
echo "top 10 lagu terbaik di tahun 2004"
echo "======================================================="
grep "2004" playlist.csv | sort -t',' -k15 -r -n | head -n 10
echo "=======================================================\n"
echo "top 5 lagu terbaik ibu Sri"
echo "======================================================="
grep "Sri" playlist.csv | sort -t',' -k15 -r -n | tail -n 5
echo "=======================================================\n"
```

## soal-1a

<p>1a. Farrel ingin memasukan lagu yang bagus dengan genre hip hop. Oleh karena itu, Farrel perlu melakukan testi terlebih dahulu. Tampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity.<p>

```bash
grep "hip hop" playlist.csv | sort -t',' -k15 -n -r | head -n 5
```

1. `grep "hip hop" playlist.csv` Ini akan mencari baris-baris di dalam file playlist.csv yang mengandung kata "hip hop".
1. `sort -t',' -k15 -n -r` Ini akan mengurutkan hasil pencarian berdasarkan kolom ke-15 (kolom popularity) secara numerik dari tertinggi ke terendah.
1. `head -n 5` Ini akan menampilkan 5 baris pertama dari hasil pengurutan, yaitu lagu-lagu hip hop dengan popularity tertinggi.

## soal-1b

<p>1b. Karena Farrel merasa kurang dengan lagu tadi, dia ingin mencari 5 lagu yang paling rendah diantara lagu ciptaan John Mayer 	berdasarkan popularity.<p>

```bash
grep "John Mayer" playlist.csv | sort -t',' -k15  | head -n 5
```

1. `grep "John Mayer" playlist.csv` Ini akan mencari baris-baris di dalam file playlist.csv yang mengandung kata "John Mayer".
1. `sort -t',' -k15` Ini akan mengurutkan hasil pencarian berdasarkan kolom ke-15 (kolom popularity) secara leksikografis (berdasarkan urutan karakter).
1. `head -n 5` Ini akan menampilkan 5 baris pertama dari hasil pengurutan, yaitu lagu-lagu John Mayer dengan popularity terendah.

## soal-1c

<p>1c. Karena Farrel takut lagunya kurang enak didengar dia ingin mencari lagu lagi, cari 10 lagu pada tahun 2004 dengan rank popularity tertinggi<p>

```bash
grep "2004" playlist.csv | sort -t',' -k15 -r -n | head -n 10
```

1. `grep "2004" playlist.csv` Ini akan mencari baris-baris di dalam file playlist.csv yang mengandung kata "2004".
1. `sort -t',' -k15 -r -n` Ini akan mengurutkan hasil pencarian berdasarkan kolom ke-15 (kolom popularity) secara numerik dari terendah ke tertinggi.
1. `head -n 10` Ini akan menampilkan 10 baris pertama dari hasil pengurutan, yaitu lagu-lagu dari tahun 2004 dengan popularity tertinggi.

## soal-1d

<p>1d. Farrel sangat suka dengan lagu paling keren dan edgy di dunia. Lagu tersebut diciptakan oleh ibu Sri. Bantu Farrel mencari lagu tersebut dengan kata kunci nama pencipta lagu tersebut. <p>

```bash
grep "Sri" playlist.csv | sort -t',' -k15 -r -n | tail -n 5
```

1. `grep "Sri" playlist.csv` Ini akan mencari baris-baris di dalam file playlist.csv yang mengandung kata "Sri".
1. `sort -t',' -k15 -r -n` Ini akan mengurutkan hasil pencarian berdasarkan kolom ke-15 (kolom popularity) secara numerik dari tertinggi ke terendah.
1. `head -n 5` Ini akan menampilkan 5 baris terakhir dari hasil pengurutan, yaitu lagu-lagu dari pencipta "Ibu Sri" dengan popularity tertinggi. Karena pengurutannya dari tertinggi ke terendah, maka yang ditampilkan adalah lagu-lagu dengan popularity terendah dari "Ibu Sri".

# Output

Berikut hasil dari bash script playlist_keren.sh

![Screenshot 2023-09-29 184112](https://github.com/J0see1/KPP-BMS/assets/134209563/7bf9c9a9-8282-42b9-b207-884be7ed87f6)

# Soal 2

Shinichi merupakan seorang detektif SMA yang kembali menjadi anak kecil karena ulah organisasi hitam. Dengan tubuhnya yang mengecil, Shinichi tidak dapat menggunakan identitas lamanya sehingga harus membuat identitas baru. Selain itu, ia juga harus membuat akun baru dengan identitas nya saat ini. Bantu Shinichi membuat program register dan login agar Shinichi dapat dengan mudah membuat semua akun baru yang ia butuhkan.

- Shinichi akan melakukan register menggunakan email, username, serta password. Username yang dibuat bebas, namun email bersifat unique.
- Shinichi khawatir suatu saat nanti Ran akan curiga padanya dan dapat mengetahui password yang ia buat. Maka dari itu, Shinichi ingin membuat password dengan tingkat keamanan yang tinggi.
  1.  Password tersebut harus di encrypt menggunakan base64
  1.  Password yang dibuat harus lebih dari 8 karakter
  1.  Harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil
  1.  Password tidak boleh sama dengan username
  1.  Harus terdapat paling sedikit 1 angka
  1.  Harus terdapat paling sedikit 1 simbol unik
- Karena Shinichi akan membuat banyak akun baru, ia berniat untuk menyimpan seluruh data register yang ia lakukan ke dalam folder users file users.txt. Di dalam file tersebut, terdapat catatan seluruh email, username, dan password yang telah ia buat.
- Shinichi juga ingin program register yang ia buat akan memunculkan respon setiap kali ia melakukan register. Respon ini akan menampilkan apakah register yang dilakukan Shinichi berhasil atau gagal
- Setelah melakukan register, Shinichi akan langsung melakukan login untuk memastikan bahwa ia telah berhasil membuat akun baru. Login hanya perlu dilakukan menggunakan email dan password.
- Ketika login berhasil ataupun gagal, program akan memunculkan respon di mana respon tersebut harus mengandung username dari email yang telah didaftarkan.
  - Ex:
    1. LOGIN SUCCESS - Welcome, [username]
    1. LOGIN FAILED - email [email] not registered, please register first
- Shinichi juga mencatat seluruh log ke dalam folder users file auth.log, baik login ataupun register.
  1.  Format: [date] [type] [message]
  1.  Type: REGISTER SUCCESS, REGISTER FAILED, LOGIN SUCCESS, LOGIN FAILED
  1.  Ex:
      - [23/09/17 13:18:02] [REGISTER SUCCESS] user [username] registered successfully
      - [23/09/17 13:22:41] [LOGIN FAILED] ERROR Failed login attempt on user with email [email]

# Analisis

Praktikan diinstruksikan untuk membuat 2 bash script yaitu, **register.sh** dan **login.sh**. Di mana user dapat melakukan registrasi dahulu pada script `register.sh` dengan beberapa ketentuan seperti email yang harus unik, password lebih dari 8 huruf kemudian tidak sama dengan username dan harus setidaknya memiliki satu huruf kapital, huruf kecil, angka, dan simbol unik. Kemudian inputan akun akan dimasukkan ke dalam file users.txt di dalam folder users dan log dari pendaftaran akun akan dicatat juga di file auth.log yang berada di folder users. Setelah registrasi, user dapat melakukan login pada script `login.sh` menggunakan email dan password yang sudah diinputkan tadi.

## \***\*_register.sh_\*\***

script bash yang digunakan untuk melakukan pendaftaran akun

```bash
#!/bash/bin

# Mengecek apakah direktori 'users' sudah ada atau belum
if [ ! -d "users" ]; then
    mkdir users
fi

# Mengecek apakah pada foler 'users' sudah ada file 'users.txt'
if [ ! -f "users/users.txt" ]; then
    touch users/users.txt
fi

#cek panjang password lebih dari 8 huruf
check_length(){
if [ ${#password} -ge 8 ]; then
  return 0
 else
  return 1
fi
}

#cek apakah password = user
check_username(){
if [ ${#password} == ${#username} ]; then
  return 1
 else
  return 0
fi
}

#cek apakah password mengandung setidaknya satu huruf kecil,huruf besar, angka, dan simbol speasial
check_character(){
  if [[ $password =~ [A-Z] ]] && [[ $password =~ [a-z] ]] && [[ $password =~ [0-9] ]] && [[ $password =~ [\W_] ]]; then
    return 0
  else
    return 1
  fi
}

while true; do
    echo "Masukkan email :"
    read email

    if grep -q "^$email" users/users.txt; then
        echo "Email sudah terdaftar. Silakan gunakan email lain."
    else
	break
    fi
done

echo "masukan username :"
read username

  while true; do
    echo "Masukkan password :"
    read password

    if check_length && check_username && check_character; then
        echo "Akun sudah tercatat"
        encrypted_password=$(echo -n "$password" | base64)
        mkdir -p users
        echo "$email $username $password $encrypted_password" >> users/users.txt

        log="[`date +'%d/%m/%y %H:%M:%S'`] [REGISTER SUCCESS] user $username terdaftar dengan sukses"
        echo "$log" >> users/auth.log

        break
    else
        echo "Silahkan ulangi input password"
    fi
done
```

### Penjelasan tiap line fungsi:

- Mengecek apakah direktori 'users' sudah ada atau belum:

```bash
if [ ! -d "users" ]; then
    mkdir users
fi
```

Bagian ini memeriksa apakah direktori 'users' sudah ada atau belum. Jika belum, maka akan dibuat direktori tersebut.

- Mengecek apakah pada folder 'users' sudah ada file 'users.txt':

```bash
if [ ! -f "users/users.txt" ]; then
    touch users/users.txt
fi
```

Bagian ini memeriksa apakah file 'users.txt' sudah ada di dalam direktori 'users'. Jika belum, maka akan dibuat file tersebut.

- Fungsi check_length:

```bash
check_length(){
    if [ ${#password} -ge 8 ]; then
        return 0
    else
        return 1
    fi
}
```

Ini adalah sebuah fungsi untuk memeriksa apakah panjang password lebih dari atau sama dengan 8 karakter. Jika iya, maka fungsi ini mengembalikan nilai 0 atau true, jika tidak, mengembalikan nilai 1 atau false.

- Fungsi check_username:

```bash
check_username(){
    if [ ${#password} == ${#username} ]; then
        return 1
    else
        return 0
    fi
}
```

Fungsi ini memeriksa apakah panjang password sama dengan panjang username. Jika iya, maka fungsi ini mengembalikan nilai false, jika tidak, mengembalikan nilai true.

- Fungsi check_character:

```bash
check_character(){
    if [[ $password =~ [A-Z] ]] && [[ $password =~ [a-z] ]] && [[ $password =~ [0-9] ]] && [[ $password =~ [\W_] ]]; then
        return 0
    else
        return 1
    fi
}
```

Fungsi ini memeriksa apakah password mengandung setidaknya satu huruf besar, satu huruf kecil, satu angka, dan satu simbol khusus. Jika iya, maka fungsi ini mengembalikan nilai true, jika tidak, mengembalikan nilai false.

- Loop untuk meminta email:

```bash
while true; do
    echo "Masukkan email :"
    read email

    if grep -q "^$email" users/users.txt; then
        echo "Email sudah terdaftar. Silakan gunakan email lain."
    else
        break
    fi
done
```

Program akan terus meminta user untuk memasukkan email sampai email yang dimasukkan belum terdaftar.

- Meminta username:

```bash
echo "masukan username :"
read username
```

Meminta user untuk memasukkan username

Loop untuk meminta password:

```bash
  while true; do
    echo "Masukkan password :"
    read password

    if check_length && check_username && check_character; then
        echo "Akun sudah tercatat"
        encrypted_password=$(echo -n "$password" | base64)
        mkdir -p users
        echo "$email $username $password $encrypted_password" >> users/users.txt

        log="[`date +'%d/%m/%y %H:%M:%S'`] [REGISTER SUCCESS] user $username terdaftar dengan sukses"
        echo "$log" >> users/auth.log

        break
    else
        echo "Silahkan ulangi input password"
    fi
done
```

Setelah mendapatkan password dari pengguna, program memeriksa apakah password tersebut memenuhi tiga kriteria keamanan dengan memanggil fungsi check_length, check_username, dan check_character. Jika password memenuhi semua kriteria, maka program akan melakukan langkah-langkah berikut:

1. Menampilkan pesan "Akun sudah tercatat".
1. Mengenkripsi password menggunakan base64 dan menyimpannya dalam variabel encrypted_password.
1. Membuat direktori 'users' jika belum ada.
1. Menyimpan informasi pengguna (email, username, password, dan password terenkripsi) ke dalam file 'users.txt'.
1. Mencatat log registrasi yang mencakup waktu registrasi dan nama pengguna ke dalam file 'auth.log'.
1. Menghentikan loop menggunakan perintah break.

Jika password tidak memenuhi salah satu dari kriteria keamanan, program akan menampilkan pesan "Silahkan ulangi input password" dan meminta pengguna untuk memasukkan password kembali.

Selama loop ini berjalan, pengguna akan terus diminta untuk memasukkan password hingga memenuhi kriteria keamanan yang ditetapkan.

## \***\*_login.sh_\*\***

Merupakan script yang akan digunakan untuk melakukan login.

```bash
#!/bin/bash

echo "Masukkan email :"
read email

echo "Masukan password :"
read password

# Memeriksa apakah email ada dalam file users.txt
if grep -q "$email" users/users.txt && grep -q "$password" users/users.txt ; then

# Mengambil nilai username yang sesuai dengan email yang sudah diinputkan pada file users.txt, kemudian menyimpannya di variabel username
    username=$(grep "$email" users/users.txt | cut -d ' ' -f 2)
    echo "LOGIN SUCCESS - Welcome, $username"

         log="[`date +'%d/%m/%y %H:%M:%S'`] [LOGIN SUCCESS] user $username berhasil login"
         echo "$log" >> users/auth.log
else
    echo "LOGIN FAILED - email $email tidak terdaftar, silakan daftar terlebih dahulu"

         log="[`date +'%d/%m/%y %H:%M:%S'`] [LOGIN FAILED] ERROR Upaya login gagal untuk email $email"
        echo "$log" >> users/auth.log
fi
```

- Memeriksa apakah email dan password ada dalam file users.txt:

```bash
if grep -q "$email" users/users.txt && grep -q "$password" users/users.txt ; then

```

Program akan menggunakan grep untuk mencari apakah email dan password yang dimasukkan oleh pengguna ada di dalam file users.txt. Jika kedua kondisi terpenuhi, artinya email dan password cocok, maka program akan melanjutkan.

- Mengambil nilai username yang sesuai dengan email:

```bash
username=$(grep "$email" users/users.txt | cut -d ' ' -f 2)

```

Program akan mencari baris dalam file users.txt yang mengandung email yang sesuai, kemudian menggunakan cut untuk memisahkan kolom-kolom dan mengambil kolom kedua, yaitu username. Nilai username tersebut akan disimpan dalam variabel username.

- Menampilkan pesan selamat datang dan mencatat log login berhasil:

```bash
    echo "LOGIN SUCCESS - Welcome, $username"

        log="[`date +'%d/%m/%y %H:%M:%S'`] [LOGIN SUCCESS] user $username berhasil login"
        echo "$log" >> users/auth.log
```

Jika email dan password cocok, program akan menampilkan pesan selamat datang dengan username pengguna yang sesuai dengan isi di file users.txt. Program kemudian akan mencatat log yang mencakup waktu login berhasil dan nama pengguna ke dalam file 'auth.log'.

- Else (jika email atau password tidak cocok):

```bash
else
    echo "LOGIN FAILED - email $email tidak terdaftar, silakan daftar terlebih dahulu"
    log="[`date +'%d/%m/%y %H:%M:%S'`] [LOGIN FAILED] ERROR Upaya login gagal untuk email $email"
    echo "$log" >> users/auth.log
fi

```

Jika email atau password tidak cocok, program akan menampilkan pesan bahwa login gagal karena email tidak terdaftar. Selain itu, program akan mencatat log login gagal dengan mencakup waktu dan email yang gagal login. <br/>

# Output

Berikut output-output dari tiap program :

## Register.sh

![register berhasil](https://github.com/J0see1/KPP-BMS/assets/134209563/91abfb73-048c-4e78-9e49-b920456ca1aa)

## Login.sh

![login berhasil](https://github.com/J0see1/KPP-BMS/assets/134209563/170c2015-220d-450a-a8c2-ac70ac523799)

## Contoh isi dari file 'users.txt' dan 'auth.log'

- isi dari auth.log

![isi auth log](https://github.com/J0see1/KPP-BMS/assets/134209563/72959483-2031-4a5b-a914-0381c124388c) <br/>

- isi dari users.txt

![isi users txt](https://github.com/J0see1/KPP-BMS/assets/134209563/f49c9cb1-8cca-4f74-81fb-2cd8dfcc5dd7)


# Soal 3
Aether adalah seorang gamer yang sangat menyukai bermain game Genshin Impact. Karena hobinya, dia ingin mengoleksi foto-foto karakter Genshin Impact. Suatu saat Pierro memberikannya sebuah Tautan yang berisi koleksi kumpulan foto karakter dan sebuah clue yang mengarah ke endgame. Ternyata setiap nama file telah dienkripsi dengan menggunakan base64. Karena penasaran dengan apa yang dikatakan piero, Aether tidak menyerah dan mencoba untuk mengembalikan nama file tersebut kembali seperti semula.
1. Aether membuat script bernama genshin.sh, untuk melakukan unzip terhadap file yang telah diunduh dan decode setiap nama file yang terenkripsi dengan base64 . Karena pada file list_character.csv terdapat data lengkap karakter, Aether ingin merename setiap file berdasarkan file tersebut. Agar semakin rapi, Aether mengumpulkan setiap file ke dalam folder berdasarkan region tiap karakter
Format: Nama - Region - Elemen - Senjata.jpg
2. Karena tidak mengetahui jumlah pengguna dari tiap senjata yang ada di folder "genshin_character".Aether berniat untuk menghitung serta menampilkan jumlah pengguna untuk setiap senjata yang ada
Format: [Nama Senjata] : [total]
	 Untuk menghemat penyimpanan. Aether menghapus file - file yang tidak ia gunakan, yaitu genshin_character.zip, list_character.csv, dan genshin.zip
3. Namun sampai titik ini Aether masih belum menemukan clue endgame yang disinggung oleh Pierro. Dia berfikir keras untuk menemukan pesan tersembunyi tersebut. Aether membuat script baru bernama find_me.sh untuk melakukan pengecekan terhadap setiap file tiap 1 detik. Pengecekan dilakukan dengan cara meng-ekstrak tiap gambar dengan menggunakan command steghide. Dalam setiap gambar tersebut, terdapat sebuah file txt yang berisi string. Aether kemudian mulai melakukan dekripsi dengan base64 pada tiap file txt dan mendapatkan sebuah url. Setelah mendapatkan url yang ia cari, Aether akan langsung menghentikan program find_me.sh serta mendownload file berdasarkan url yang didapatkan.
4. Dalam prosesnya, setiap kali Aether melakukan ekstraksi dan ternyata hasil ekstraksi bukan yang ia inginkan, maka ia akan langsung menghapus file txt tersebut. Namun, jika itu merupakan file txt yang dicari, maka ia akan menyimpan hasil dekripsi-nya bukan hasil ekstraksi. Selain itu juga, Aether melakukan pencatatan log pada file image.log untuk setiap pengecekan gambar
    - Format: [date] [type] [image_path]
    - Ex: 
        -[23/09/11 17:57:51] [NOT FOUND] [image_path]
        -[23/09/11 17:57:52] [FOUND] [image_path]
4. Hasil akhir:
    - genshin_character
    - find_me.sh
    - genshin.sh
    - image.log
    - [filename].txt
    - [image].jpg

# Analisis genshin.sh

![genshin wget](https://github.com/J0see1/KPP-BMS/assets/134209563/e54ba242-f1f4-4296-9bac-30efbac3b2c7)

wget -O genshin.zip "https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2"
Perintah wget ini digunakan untuk mengunduh file dengan nama `genshin.zip` dari URL yang diberikan. File tersebut diunduh dari Google Drive menggunakan ID file yang disediakan. 

![unzip](https://github.com/J0see1/KPP-BMS/assets/134209563/3819fe1e-8ba6-4a5b-ae33-b8082a20d923)

unzip genshin.zip
unzip genshin_character.zip
Unzip digunakan untuk mengekstrak isi file dari genshin.zip dan genshin_character.zip. Setelah perintah ini dijalankan, isi dari file tersebut akan diekstrak dan ditempatkan dalam direktori saat ini. 

![list](https://github.com/J0see1/KPP-BMS/assets/134209563/e6054b80-514d-4013-b6db-939ac6b24bd7)

- while IFS=',' read -r nama_region elemen senjata; do
for file in *.jpg; do : loop yang membaca baris-baris dari file list_character.csv yang dipisahkan oleh tanda koma (,). Variabel nama_region, elemen, dan senjata akan diisi dengan nilai dari masing-masing kolom pada setiap baris.
- for encoded_file in genshin_character/*.jpg; do :  loop yang akan memeriksa setiap file dengan ekstensi .jpg di dalam direktori genshin_character.
encoded_name=$(basename "$encoded_file" .jpg) & decoded_name=$(echo "$encoded_name" | base64 -d) : digunakan untuk mendekripsi nama file. basename digunakan untuk mengambil nama file tanpa ekstensi, dan kemudian kita mendekripsi nama tersebut dengan base64 -d.
- if [ "$decoded_name" = "$nama_region" ]; then 
kondisi untuk memeriksa apakah nama karakter yang telah didekripsi sama dengan nama_region yang sedang dibaca dari file list_character.csv.
- new_file_name="$nama_region - $elemen - $senjata.jpg"
mv "$encoded_file" "genshin_character/$new_file_name"
Jika nama karakter cocok, kita membuat nama file baru sesuai dengan format yang diinginkan ($nama_region - $elemen - $senjata.jpg) dan menggunakan mv untuk mengganti nama file tersebut.
- character_data=$(awk -F"," -v name="$decoded_name" '$1 == name {print}' list_character.csv)
menggunakan awk untuk mencari data karakter yang sesuai dengan nama karakter yang telah didekripsi dari file list_character.csv. -F"," digunakan untuk mengatur tanda pemisah kolom, dan -v name="$decoded_name" digunakan untuk mengatur variabel name dalam awk.
- if [ -n "$character_data" ]; then
kondisi untuk memeriksa apakah data karakter telah ditemukan.
- region=$(echo "$character_data" | awk -F"," '{print $2}')
new_name=$(echo "$character_data" | awk -F"," '{print $1"-"$2"-"$3"-"$4}' | sed 's/[^A-Za-z0-9._-]//g')
mkdir -p "genshin_character/$region"
mv "genshin_character/$new_file_name" "genshin_character/$region/$new_name"
Jika data karakter telah ditemukan, kita mengambil kolom kedua dari data karakter sebagai region, kemudian membuat nama baru dengan format yang diinginkan ($1"-"$2"-"$3"-"$4) dan menghilangkan karakter yang tidak diizinkan dengan sed. Selanjutnya, kita membuat direktori sesuai dengan region dan memindahkan file yang telah diubah namanya ke direktori tersebut.
- done < list_character.csv
Ini adalah akhir dari loop yang membaca file list_character.csv.

![declare](https://github.com/J0see1/KPP-BMS/assets/134209563/7e451dd8-e3cb-4fef-946f-fb2320dfbfd3)

- declare -A weapon_counts
mendeklarasikan variabel `weapon_counts` sebagai associative array. 
- for region_dir in genshin_character/*; do
memulai loop yang akan berjalan melalui semua folder di dalam direktori `genshin_character`.
- if [ -d "$region_dir" ]; then
kondisi yang memeriksa apakah item saat ini yang sedang di-loop adalah direktori (`-d`).
- region_name=$(basename "$region_dir")
Membuat nama folder (`region_dir`) dengan menggunakan `basename` untuk digunakan nanti dalam menghitung jumlah pengguna senjata dalam region tersebut.
- for character_file in "$region_dir"/*; do
Ini adalah loop yang akan berjalan melalui semua file dalam setiap folder region.
- if [ -f "$character_file" ]; then
kondisi yang memeriksa apakah item saat ini yang di-loop adalah sebuah file (`-f`).
- character_name=$(basename "$character_file" .jpg)
Di sini, Anda mengambil nama file karakter tanpa ekstensi `.jpg` dengan menggunakan `basename`.
- weapon_name=$(echo "$character_name" | awk -F"-" '{print $3}')
mengambil nama senjata dari nama karakter dengan menggunakan `awk` dan membagi nama karakter berdasarkan tanda `-` (`-F"-"`) dan kemudian mengambil kolom ketiga (indeks 3).
- if [ -n "$weapon_name" ]; then
Ini adalah kondisi yang memeriksa apakah `weapon_name` tidak kosong.
- ((weapon_counts["$weapon_name"]++))
menambahkan satu ke nilai yang terkait dengan `weapon_name` dalam associative array `weapon_counts`. 

![jumlah pengguna](https://github.com/J0see1/KPP-BMS/assets/134209563/0b2c455d-da4e-43fa-a9ae-7d8131455e1a)

- echo "Jumlah pengguna untuk setiap senjata:"
pesan teks `"Jumlah pengguna untuk setiap senjata:"` akan dicetak ke layar sebagai judul atau header.
- for weapon in "${!weapon_counts[@]}"; do
memulai loop `for` yang akan menjalankan pernyataan berikut untuk setiap senjata yang ada dalam associative array `weapon_counts`. `${!weapon_counts[@]}` digunakan untuk mendapatkan daftar semua kunci (senjata) yang ada dalam associative array.
- echo "[$weapon] : ${weapon_counts[$weapon]}"
mencetak informasi jumlah pengguna untuk setiap senjata. `${weapon_counts[$weapon]}` digunakan untuk mengakses nilai (jumlah pengguna) yang terkait dengan senjata yang sedang di-loop. `[$weapon]` digunakan untuk mencetak nama senjata dalam tanda kurung siku.

![delete](https://github.com/J0see1/KPP-BMS/assets/134209563/f3fb59de-25e2-419b-8c95-c129e872c1d6)

rm genshin.zip

rm genshin_character.zip

rm list_character.csv
- Perintah ‘rm’ digunakan untuk menghapus suatu file dari direktori 

# Output genshin.sh

![output genshin](https://github.com/J0see1/KPP-BMS/assets/134209563/0a212327-7cc9-47a2-801c-c7fdbccd8376)

# Analisis findme.sh

![extract](https://github.com/J0see1/KPP-BMS/assets/134209563/204d99d0-3bd3-45cb-a353-3aecf0a68f6e)

extract_message() {
"extract_message() {": deklarasi fungsi "extract_message" digunakan untuk mengekstraksi pesan dari gambar dan memastikan apakah pesan tersebut memiliki URL. 

![for find me](https://github.com/J0see1/KPP-BMS/assets/134209563/15897a3b-9be3-4e19-8a21-68da7935ec3d)

- "for file in *.jpg; do": loop "for" akan digunakan untuk setiap file dengan ekstensi ".jpg" yang ada dalam direktori saat ini.
- Kondisi "if [ -f "$file" ]; then" digunakan untuk menguji apakah file yang sedang diproses benar-benar ada. Penggunaan "-f" menguji keberadaan file.
- Perintah "echo "[$(date +"%d/%m/%y %T")] [EXTRACTING] [$file]" >> image.log" digunakan untuk mencetak pesan ke file "image.log". Pesan ini mengandung informasi tentang proses ekstraksi, seperti timestamp dan nama file yang diekstrak.
- Perintah "steghide extract -sf "$file" -p "" -xf extracted.txt" digunakan untuk menjalankan ekstraksi menggunakan perangkat lunak "steghide". Perintah ini mencoba mengekstrak pesan tersembunyi dari file gambar dengan nama "$file" dan menyimpannya dalam file "extracted.txt". Opsi "-p" menunjukkan bahwa passphrase kosong digunakan

![if findme](https://github.com/J0see1/KPP-BMS/assets/134209563/75eae78e-536e-4060-a30e-1da8caa051fa)

- Kondisi "if [$? -eq 0 ]; then" mengevaluasi keberhasilan perintah ekstraksi "steghide". Nilai "$?" menyimpan kode dari perintah sebelumnya, dan "-eq 0" menunjukkan keberhasilan.
- echo "[$(date +"%d/%m/%y %T")] [FOUND] [$file]" >> image.log : Ini adalah perintah yang mencetak pesan ke file image.log jika ekstraksi berhasil dan menunjukkan bahwa pesan tersembunyi ditemukan dalam file gambar.

![decode findme](https://github.com/J0see1/KPP-BMS/assets/134209563/77a7f1e1-b3cc-46f0-a905-c534c7ba7811)

Perintah "decoded_message=$(base64 -d < extracted.txt | tr -d '\0' 2>/dev/null)" digunakan untuk mendekripsi pesan yang diekstrak dari base64. Hasil dekripsi disimpan dalam variabel "decoded_message", dan karakter null yang mungkin ada dalam pesan dihapus dengan perintah "tr -d '\0' 2>/dev/null".

![if findme 2](https://github.com/J0see1/KPP-BMS/assets/134209563/49da98c1-b2de-430b-9581-e7abd16de7fd)

- Kondisi "if [[ $decoded_message =~ ^http ]]; then" menentukan apakah pesan yang dienkripsi memiliki URL. Jika pesan dimulai dengan "http," maka itu dianggap sebagai URL.
- Perintah "echo "URL ditemukan: $decoded_message" mencetak URL yang ditemukan dalam pesan ke layar.

![wget findme](https://github.com/J0see1/KPP-BMS/assets/134209563/f63a6917-6ed1-478f-a2fc-338865750406)

- Perintah "wget" dapat digunakan untuk mengunduh file berdasarkan URL yang ditemukan dalam pesan.
- exit 0 digunakan untuk menghentikan skrip dalam kasus di mana URL ditemukan dan file diunduh.


![extrac findme](https://github.com/J0see1/KPP-BMS/assets/134209563/9699f8d0-9a60-4e26-83cf-5fc137dd488c)

- rm extracted.txt 
- fi
- else
- Digunakan unruk menghapus file extracted.txt alam kasus di mana pesan yang diekstrak tidak memiliki URL atau ekstraksi gagal.

![extrac findme sdal](https://github.com/J0see1/KPP-BMS/assets/134209563/228869fd-d06b-4d32-8c23-1b9f09b6a535)

- Loop "while true; do" terus berjalan sampai dihentikan secara manual.
- fungsi "extract_message" memungkinkan proses ekstraksi pesan dari gambar.
- "sleep 1" menghentikan skrip selama 1 detik sebelum memulai ekstraksi lagi, membatasi seberapa sering skrip melakukan pengecekan dan ekstraksi.

# Output findme.sh

![output findme](https://github.com/J0see1/KPP-BMS/assets/134209563/6b82e1db-aba7-4817-8d81-870d9443929c)

# Soal 4
- a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2023-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20230131150000.log.

![soal 4a](https://github.com/J0see1/KPP-BMS/assets/134209563/6ff3b609-4a59-4bd2-b3e8-29129a746bb0)

```bash
#!/bin/bash

#melihat pengguna user sekarang
user=$dave

log_dir="/home/dave/log/"

timestamp=$(date +"%Y%m%d%H%M%S")

#menyimpan file dalam log
log_file="${log_dir}metrics_${timestamp}.log"


#menggunakan perintah free untuk mendapat info swap dan RAM
ram=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7}')
swap=$(free -m | awk 'NR==3 {print $2","$3","$4}')

#menyimpan ukuran directory yang ditentukan
target_path="/home/dave/"
path_size=$(du -sh "$target_path" | awk '{print $1}')

#memberikan output dalam format csv
echo "timestamp $timestamp" >> "$log_file"
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "$log_file"
echo "$ram,$swap,$target_path,$path_size" >> "$log_file"

chmod 600 "$log_file"

#crontab config
#* * * * * /Home/dave/praktikum-sisop/soal_4/minute_log.sh

```

1. masuk dalam Terminal Linux ubuntu
1. buatlah sebuah scriptbash pada teks editor vim dengan judul minute_log.sh
1. buatlah suatu direktori tempat penyimpanan file log yang nantinya akan dibuat dengan perintah log_dir=”/home/dave/log”
1. selanjutnya buat sebuah timestamp yang nantinya akan digunakan sebagai penghitung waktu dalam script dengan perintah timestamp=$(date +”%Y%m%d%H%M%S”)
1. lalu buatlah sebuah format penyimpanan file log dengan perintah log_file=”${log_dir}metrics_${timestamp}.log”
1. gunakan perintah free -m untuk mendapat info ram dan swap dalam megabyte
1. untuk ram gunakan perintah ram=$(free -m | awk ‘NR=2 {print $2”,”$3”,”$4”,”$5”,”$6”,”$7}’)
1. untuk swap gunakan perintah swap=$(free -m | awk ‘NR=3 {print $2”,”$3”,”$4}’)
1. tentukan directory yang akan monitoring menggunakan perintah target_path=”/home/dave”
1. untuk melihat size yang ada dalam directory tersebut menggunakan perintah path_size=$(du -sh “$target_path” | awk ’{print $1}’)
1. berikan outputnya dalam format csv menggunakan perintah echo “timestamp $timestamp” >> “$log_file”
1. echo “mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size ” >> “$log_file”
1. echo “$ram,$swap,$target_path,$path_size” >> “$log_file”
1. selanjutnya buatlah perintah supaya script hanya bisa dibaca dan dijalankan oleh user menggunakan perintah chmod 600 “$log_file”

- b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit. 

![otuput 4b](https://github.com/J0see1/KPP-BMS/assets/134209563/20640344-4f81-498c-a5d3-b4e02652b41d)

diatas merupakan tahapan untuk membuat sebuah script minute_log.sh, yang dimana script ini akan jalan secara otomatis setiap menit dengan menggunakan crontab. sebelumnya harus menginstal crontab terlebih dahulu, selanjutnya bukalah crontab dengan perintah cronetab -e
masukan perintah * * * * * /home/dave/praktikum-sisop/soal_4/minute_log.sh maka secara otomatis script yang sudah kita kerjakan akan dijalankan secara otomatis setiap menit.

- c.  	Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2023013115.log dengan format metrics_agg_{YmdH}.log 

```bash
#!/bin/bash

#directory tempat file log tergenerate dari minute_log.sh
log_dir="/home/dave/log/"

#format timestamp YYYYMMDDHH
timestamp=$(date +"%Y%m%d%H")

#nama file aggregasi saat ini
file_agg="metrics_agg_${timestamp}.log"

#mencari semua file log yang akan diaggregasi
log_files=$(ls ${log_dir}metrics_*.log)

#inisialisasi variabel minimum, maksimum, total, dan rata-rata
minimum=9999999
maksimum=0
total=0
count=0

#loop pada file log
for file in $log_files; do
    # Mengambil nilai dari file log
    value=$(tail -n 1 $file | awk -F',' '{print $2}')  # Ubah sesuai dengan kolom yang ingin dihitung

    # Menghitung maksimum
    if (( $(echo "$value > $maksimum" | bc -l) )); then
        maksimum=$value
    fi

    # Menghitung minimum
    if (( $(echo "$value < $minimum" | bc -l) )); then
        minimum=$value
    fi

    # Menambahkan nilai untuk rata-rata
    total=$(echo "$total + $value" | bc -l)
    ((count++))
done

# Menghitung rata-rata
average=$(echo "scale=2; $total / $count" | bc -l)

#simpan ke file aggregasi
echo "maksimum $maksimum" > "$file_agg"
echo "minimum $minimum" >> "$file_agg"
echo "average $average" >> "$file_agg"

chmod 600 "$log_files"

#crontab config
#0 1 * * * /Home/dave/praktikum-sisop_/soal_4/aggregate_minutes_to_hourly_log.sh

```

- buatlah sebuah script untuk membuat agregasi file log ke satuan jam.
1. buatlah sebuah script baru pada teks editor vim dengan nama aggregate_minutes_to_hourly_log.sh
1. buat perintah untuk menunjukan dimana directory penyimpanan file log dengan perintah log_dir="/home/dave/log/"
1. buatlah sebuah timestamp dengan format YYYYmmddHH dengan perintah timestamp=$(date +"%Y%m%d%H")
1. buatlah nama file lognya dengan perintah  file_agg="metrics_agg_${timestamp}.log"
1. carilah file log yang akan diagregasi dalam directory penyimpanan file - file log sebelumnya dengan perintah log_files=$(ls ${log_dir}metrics_*.log)
1. inisialisasi variabel minimum maksimum total dengan perintah 
   - minimum=9999999
   - maksimum=0
   - total=0
   - count=0
1. ambil nilai metrics dari file dengan perintah loop for file in $log_files; do
1. inisialisasi jumlah dari metrics  value=$(tail -n 1 $file | awk -F',' '{print $2}')
1. hitung nilai maksimum

 - if (( $(echo "$value > $maksimum" | bc -l) )); then
        maksimum=$value
    fi
1. hitung nilai minimum dengan perintah
  
 - if (( $(echo "$value < $minimum" | bc -l) )); then
        minimum=$value
    fi
1. tambah nilai untuk menghitung rata-rata dengan perintah
 total=$(echo "$total + $value" | bc -l)
    ((count++))
done

1. hitung rata-rata dengan perintah average=$(echo "scale=2; $total / $count" | bc -l)
1. simpan semua value yang sudah dihitung sebelumnya dalam file aggregasi yang sudah dibuat sebelumnya dengan perintah 

echo "maksimum $maksimum" > "$file_agg"

echo "minimum $minimum" >> "$file_agg"
echo "average $average" >> "$file_agg"

1. jangan lupa buat menambahkan chmod 600 “$log_file” agar file log yang sudah dijalankan hanya dapat dilihat oleh user

