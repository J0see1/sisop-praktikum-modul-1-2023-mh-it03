#!/bin/bash

echo "Masukkan email :"
read email

echo "Masukan password :"
read password

# Memeriksa apakah email ada dalam file users.txt
if grep -q "$email" users/users.txt && grep -q "$password" users/users.txt ; then

# Mengambil nilai username yang sesuai dengan email yang sudah diinputkan pada file users.txt, kemudian menyimpannya di variabel username
    username=$(grep "$email" users/users.txt | cut -d ' ' -f 2)
    echo "LOGIN SUCCESS - Welcome, $username"

	 log="[`date +'%d/%m/%y %H:%M:%S'`] [LOGIN SUCCESS] user $username berhasil login"
    	 echo "$log" >> users/auth.log
else
    echo "LOGIN FAILED - email $email tidak terdaftar, silakan daftar terlebih dahulu"

	 log="[`date +'%d/%m/%y %H:%M:%S'`] [LOGIN FAILED] ERROR Upaya login gagal untuk email $email"
    	echo "$log" >> users/auth.log
fi
