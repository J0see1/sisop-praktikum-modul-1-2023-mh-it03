#!/bash/bin

# Mengecek apakah direktori 'users' sudah ada atau belum
if [ ! -d "users" ]; then
    mkdir users
fi

# Mengecek apakah pada foler 'users' sudah ada file 'users.txt'
if [ ! -f "users/users.txt" ]; then
    touch users/users.txt
fi

#cek panjang password lebih dari 8 huruf
check_length(){
if [ ${#password} -ge 8 ]; then
  return 0
 else
  return 1
fi
}

#cek apakah password = user
check_username(){
if [ ${#password} == ${#username} ]; then
  return 1
 else
  return 0
fi
}

#cek apakah password mengandung setidaknya satu huruf kecil,huruf besar, angka, dan simbol speasial
check_character(){
  if [[ $password =~ [A-Z] ]] && [[ $password =~ [a-z] ]] && [[ $password =~ [0-9] ]] && [[ $password =~ [\W_] ]]; then
    return 0
  else
    return 1
  fi
}

while true; do
    echo "Masukkan email :"
    read email

    if grep -q "^$email" users/users.txt; then
        echo "Email sudah terdaftar. Silakan gunakan email lain."
    else
        break
    fi
done

echo "masukan username :"
read username

  while true; do
    echo "Masukkan password :"
    read password

    if check_length && check_username && check_character; then
        echo "Akun sudah tercatat"
        encrypted_password=$(echo -n "$password" | base64)
        mkdir -p users
        echo "$email $username $password $encrypted_password" >> users/users.txt

        log="[`date +'%d/%m/%y %H:%M:%S'`] [REGISTER SUCCESS] user $username terdaftar dengan sukses"
        echo "$log" >> users/auth.log

        break
    else
        echo "Silahkan ulangi input password"
    fi
done
